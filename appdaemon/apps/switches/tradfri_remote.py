## Based of this config:
## https://github.com/isabellaalstrom/home-assistant-config 

## This file should be accompanied with a file called tradfri_remote.yaml
## that could look like this:
##
## colored_light:
##   module: tradfri_remote
##   class: TradfriRemote
##   event: deconz_event
##   id: remote1
##   lightId: light.light1
##   mode: color
## temperatured_light:
##   module: tradfri_remote
##   class: TradfriRemote
##   event: deconz_event
##   id: remote2
##   lightId: light.light2
##   mode: temperature
## left_right_buttons_used_for_toggle:
##   module: tradfri_remote
##   class: TradfriRemote
##   event: deconz_event
##   id: remote3
##   lightId: light.light3
##   mode: lrtoggle
##   l_entity_id: light.some_other_lamp
##   r_entity_id: switch.some_other_switch


import appdaemon.plugins.hass.hassapi as hass


# Use any of the CSS3 Color Names, could for example be
# be found here:
# https://www.cssportal.com/css3-color-names/

COLORS = (
    'blue',
    'red',
    'orange',
    'green',
    'crimson',
    'blueviolet',
    'floralwhite',
    'antiquewhite'
)

COLOR_TEMPS = (
    463,
    400,
    325
)

MIN_DIM_VALUE = 10
MAX_DIM_VALUE = 255

class TradfriRemote(hass.Hass):
    def __init__(self, *args, **kwargs):
        self.color_cnt = 0
        self.color_temp_cnt = 0
        super().__init__(*args, **kwargs)

    def _next_value(self, mode, step):
        """Steps counter and get new value for color or color temperature.
 
        Attributes:
            mode       'color' or 'temperature'
            step       Probably either 1 or -1
        """

        if mode == 'color':
            counter = self.color_cnt
            value_list = COLORS
        elif mode == 'temperature':
            counter = self.color_temp_cnt
            value_list = COLOR_TEMPS
        else:
            raise TypeError('mode should be set to "color" or "temperature"')

        # Change value of counter, but wraps it around if not an index of value_list.
        counter = (counter + step) % len(value_list)
        self.log('Stepped {} counter with a value of {}. New value: {}.'.format(
            mode, step, counter
        ))

        # Just changing to value of counter variable won't change attribute on class.
        if mode == 'color':
            self.color_cnt = counter
        elif mode == 'temperature':
            self.color_temp_cnt = counter

        return value_list[counter]

    def _change_color_or_temp_value(self, mode, light_id, value):
        """Changes color or temp value on bulb depending on mode.

        Attributes:
            mode       'color' or 'temperature'
            light_id   light_id as passed from yaml file
            value      new value to set for color or color temperature
        """

        if mode == 'color':
            self.turn_on(light_id, color_name=value)
            self.log('Sets color "{}"'.format(value))
        elif mode == 'temperature':
            self.turn_on(light_id, color_temp=value)
            self.log('Sets colortemp to {}'.format(value))
        else:
            raise TypeError('mode should be set to "color" or "temperature"')

    def _side_btn_toggle(self, side):
        """Toggle something based on which side button that is pushed.

        Attributes:
            side  'left' or 'right'
        """
        if side=='left':
            argname = 'l_entity_id'
            entity = self.args.get(argname)
        elif side=='right':
            argname = 'r_entity_id'
            entity = self.args.get(argname)
        else:
            raise ValueError('Value of side should have been either "left" or "right"')

        if entity:
            self.log('{} is present. Toggles "{}".'.format(argname, entity))
            self.toggle(entity)
        else:
            self.log('No entity for {} button specified. Should have a "{}" entry'.format(side, argname))

    def initialize(self):
        if 'event' in self.args:
            self.listen_event(self.handle_event, self.args['event'])

    def handle_event(self, event_name, data, kwargs):
        if data['id'] == self.args['id']:
            if data['event'] == 1002:
                self.log('Button toggle')
                self.toggle(self.args['lightId'])

            elif data['event'] == 2001:
                self.log('Button dim up hold')
                self.turn_on(self.args['lightId'], brightness=MAX_DIM_VALUE)

            elif data['event'] == 2002:
                self.log('Button dim up')
                prev_brightness = self.get_state(self.args['lightId'], attribute="brightness")
                if prev_brightness == None:
                    self.turn_on(self.args['lightId'], brightness = 25)
                elif prev_brightness > 205:
                    self.turn_on(self.args['lightId'], brightness = 255)
                else:
                    self.turn_on(self.args['lightId'], brightness = int(prev_brightness) + 50)

                log_template = 'Sets brightness to {}.'
                brightness = self.get_state(self.args['lightId'], attribute="brightness")
                self.log(log_template.format(brightness))

            elif data['event'] == 3001:
                self.log('Button dim down hold')
                self.turn_on(self.args['lightId'], brightness=MIN_DIM_VALUE)

            elif data['event'] == 3002:
                self.log('Button dim down')
                prev_brightness = self.get_state(self.args['lightId'], attribute="brightness")
                threshold = 51
                try:
                    if prev_brightness <= threshold:
                        self.log('{} is below threshold ({}) and will be se to MIN_DIM_VALUE ({}).'.format(
                            self.args['lightId'], threshold, MIN_DIM_VALUE
                        ))
                        self.turn_on(self.args['lightId'], brightness=MIN_DIM_VALUE)
                    else:
                        self.turn_on(self.args['lightId'], brightness=prev_brightness - 50)
                        log_template = 'Sets brightness to {}.'
                        brightness = self.get_state(self.args['lightId'], attribute="brightness")
                        self.log(log_template.format(brightness))
                except TypeError:  # prev_brightness is None when turned off and will cast TypeError.
                    self.log('{} is turned off.'.format(self.args['lightId']))

            elif data['event'] == 4002:
                self.log('Button left')
                if self.args.get('mode') == 'color' or self.args.get('mode') == 'temperature':
                    value = self._next_value(self.args['mode'], -1)
                    self._change_color_or_temp_value(self.args['mode'], self.args['lightId'], value)
                elif self.args.get('mode') == 'lrtoggle':
                    self._side_btn_toggle('left')

            elif data['event'] == 5002:
                self.log('Button right')
                if self.args.get('mode') == 'color' or self.args.get('mode') == 'temperature':
                    value = self._next_value(self.args['mode'], 1)
                    self._change_color_or_temp_value(self.args['mode'], self.args['lightId'], value)
                elif self.args.get('mode') == 'lrtoggle':
                    self._side_btn_toggle('right')
