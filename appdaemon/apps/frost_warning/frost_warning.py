import datetime
import time

import appdaemon.plugins.hass.hassapi as hass


class FrostWarning(hass.Hass):
    def __init__(self, *args, **kwargs):
        self.triggered = False
        super().__init__(*args, **kwargs)

    def initialize(self):
        trigger_times = self.args.get('trigger_times')
        reset_times = self.args.get('reset_times')
        assert isinstance(trigger_times, list)
        assert isinstance(reset_times, list)

        for time_str in trigger_times:
            runtime = datetime.datetime.strptime(time_str, '%H:%M').time() 
            self.log('Adds trigger time {}.'.format(runtime))
            self.run_daily(self.check_warning, runtime)

        for time_str in reset_times:
            runtime = datetime.datetime.strptime(time_str, '%H:%M').time()
            self.log('Adds reset time {}.'.format(runtime))
            self.run_daily(self.reset_warning, runtime)

    def check_warning(self, *args, **kwargs):
        temperatures = []
        sensors = self.args.get('sensors')
        trigger_temp = self.args.get('trigger_temp')

        self.log('Checking temperature for sensors: {}'.format(sensors))
        for entity in sensors:
            temp = self.get_state(entity=entity, attribute='Temperature')
            temperatures.append(temp)

        lowest_temp = min(temperatures)

        if lowest_temp <= trigger_temp:
            self.log('Lowest temp ({}) is below the trigger temperature ({})'.format(
                lowest_temp, trigger_temp
            ))
            self.trigger_warning()

    def trigger_warning(self):
        self.log('Trigger a warning')

        alert_entities = self.args['alert_entities']
        assert isinstance(alert_entities, list)

        color = self.args['alert_color']
        assert isinstance(color, str)
        for entity in alert_entities:
            self.turn_on(entity, color_name=color)

        self.triggered = True

    def reset_warning(self, *args, **kwargs):
        if self.triggered:
            self.log('Trigger a reset of warnings')
            alert_entities = self.args['alert_entities']
            assert isinstance(alert_entities, list)

            color = self.args['reset_color']
            assert isinstance(color, str)

            self.log('Turning all warning lights {}.'.format(self.args['reset_color']))
            for entity in alert_entities:
                self.turn_on(entity, color_name=color)

            self.run_in(self.turn_off_all, 2)
            self.triggered = False

    def turn_off_all(self, *args, **kwargs):
        alert_entities = self.args['alert_entities']

        self.log('Turning off all warning lights.')

        for entity in alert_entities:
            self.turn_off(entity)
