#!/bin/sh

set -e

YAML_FILES_DIR="$(dirname $0)/"  # Trailing slash matters for rsync
REMOTE_PATH="root@10.1.20.11:2222@hass.home:/config/"

rsync -zrltv                   \
      --prune-empty-dirs       \
      --include "*/"           \
      --include="/www/*.jpg"   \
      --include="/www/*.mp3"   \
      --include="*.yaml"       \
      --include="*/__init__.py"  \
      --include="*.py"         \
      --include="*.js"         \
      --include="*.json"       \
      --include=".gitkeep"     \
      --exclude="*"            \
      "$YAML_FILES_DIR"        \
      "$REMOTE_PATH"
